# Create your models here.
'''
Author: Mr.Sen
LastEditTime: 2021-02-15 11:01:51
Description: 
Website: https://grimoire.cn
Copyright (c) Mr.Sen All rights reserved.
'''
from django.db import models
from django.db.models.fields import CharField, EmailField, IntegerField, TextField
from django.db.models.fields.related import ForeignKey


# 存储文档
class contents(models.Model):
    title = CharField(max_length=30)
    slug = CharField(max_length=30, blank=True)  # 缩略名
    created = IntegerField()  # 创建时的时间戳
    text = TextField()  # 内容html
    rawtext = TextField() # 内容 markdown
    authorId = IntegerField()
    password = CharField(max_length=20, blank=True)
    isPublic = CharField(max_length=20, default="private")
    meta = ForeignKey("meta", on_delete=models.CASCADE)


# 存储分组数据
class meta(models.Model):
    name = CharField(max_length=30)
    slug = CharField(max_length=30, blank=True)
    count = IntegerField()
    user = ForeignKey("users", on_delete=models.CASCADE)
    lastEdit = IntegerField(default=1609430400)
    isPublic = CharField(max_length=20,default="private")


# 存储用户配置数据
class users(models.Model):
    userId = IntegerField()
    name = CharField(max_length=30)  # 昵称
    email = EmailField()
    nicName = CharField(max_length=50, default="新用户12138")
    avatar = CharField(max_length=150, default="/static/img/boy.png")
    iconfont = CharField(max_length=50, default="//at.alicdn.com/t/font_2373729_0kwlky924ul.css")
    icons = TextField(default="icon-boke@https://grimoire.cn")