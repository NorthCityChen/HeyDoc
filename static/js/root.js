/*
 * @Author: Mr.Sen
 * @LastEditTime: 2021-02-14 22:35:02
 * @Description: 
 * @Website: https://grimoire.cn
 * @Copyright (c) Mr.Sen All rights reserved.
 */
// 点击toggleBtn行为
var toggleBtn = document.querySelector(".toggleBtn-navigator");
var navigationMobile = document.querySelector(".navigation-bar-mobile");
var body = document.querySelector(".body");

function displayMenu() {
	const nodeValue = navigationMobile.attributes.class.nodeValue;
	// navigation-bar-mobile 默认是关闭的
	// .toggleBtn-navigator 默认是在中间的
	if (nodeValue == "navigation-bar-mobile") {
		navigationMobile.setAttribute(
			"class",
			"navigation-bar-mobile navigation-bar-mobile-display"
		);
		toggleBtn.setAttribute("class", "toggleBtn-navigator toggleBtn-navigator-right");
		body.setAttribute("class", "body body-hide");
	} else {
		navigationMobile.setAttribute("class", "navigation-bar-mobile");
		toggleBtn.setAttribute("class", "toggleBtn-navigator");
		body.setAttribute("class", "body");
	}
}

// 滑动隐藏toggleBtn
var oldScrollNum = 0;
window.onscroll = function() {
	const t = document.documentElement.scrollTop || document.body.scrollTop;
	if (t > oldScrollNum) {
		//下滑
		hideToggle()

	} else {
		//上拉
		displayToggle()

	}
	oldScrollNum = t;
}

function hideToggle() {
	const nodeValue = navigationMobile.attributes.class.nodeValue;

	// 滑动时先隐藏navigation-bar
	if (nodeValue == "navigation-bar-mobile navigation-bar-mobile-display") {
		navigationMobile.setAttribute("class", "navigation-bar-mobile");
		body.setAttribute("class", "body");

	}
	toggleBtn.setAttribute("class", "toggleBtn-navigator toggleBtn-navigator-left");

}

function displayToggle() {
	const nodeValue = navigationMobile.attributes.class.nodeValue;

	if (nodeValue == "navigation-bar-mobile navigation-bar-mobile-display") {
		navigationMobile.setAttribute("class", "navigation-bar-mobile");
		body.setAttribute("class", "body");
	}
	toggleBtn.setAttribute("class", "toggleBtn-navigator")
}

// 时间转换
function getLocalTime(nS) {
	var d = new Date(nS * 1000);
	return d.getFullYear() + '年' + (d.getMonth() + 1) + '月' + d.getDate() + '日'
}


function getLocalTime2(nS) {
	var d = new Date(nS * 1000);
	return d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
}