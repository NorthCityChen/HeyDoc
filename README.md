<!--
 * @Author: Mr.Sen
 * @LastEditTime: 2021-01-31 10:19:58
 * @Description: 
 * @Website: https://grimoire.cn
 * @Copyright (c) Mr.Sen All rights reserved.
-->
<h1 align="center">HeyDoc</h1>

<p align="center">一个基于Django的轻量文档管理方案</p>

<p align="center">
<img src="https://img.shields.io/badge/HeyDoc-v0.2.1-brightgreen.svg" title="Mrdoc" />
<img src="https://img.shields.io/badge/Python-3.5+-blue.svg" title="Python" />
<img src="https://img.shields.io/badge/Django-v2.2-important.svg" title="Django" />
</p>

<p align="center">
<a href="https://doc.grimoire.cn">示例站点</a> |
<a href="https://gitee.com/NorthCityChen/freeze">项目地址</a> |
</p>

## 简介

正在积极施工中...