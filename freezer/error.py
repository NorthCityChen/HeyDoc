'''
Author: Mr.Sen
LastEditTime: 2021-01-24 14:20:57
Description: 
Website: https://grimoire.cn
Copyright (c) Mr.Sen All rights reserved.
'''

from django.shortcuts import render
from database.models import users
import datetime


def isDarkMode():
    hour = datetime.datetime.now().hour
    print(hour)
    if 7 <= hour < 22:
        return False
    else:
        return True


def pageNotFound(request, exception):
    user = users.objects.get(id=request.user.id)
    ret = {
        "pageName": "首页",
        "docId": 0,
        # "range": range(len(data)),
        "alertMsg": "新建文章",
        "nickName": user.nicName,
        "avatar": user.avatar,
        "darkMod": isDarkMode(),
    }
    return render(request, "404.html", ret)


def serverErr(request):
    user = users.objects.get(id=request.user.id)
    ret = {
        "pageName": "首页",
        "docId": 0,
        # "range": range(len(data)),
        "alertMsg": "新建文章",
        "nickName": user.nicName,
        "avatar": user.avatar,
        "darkMod": isDarkMode(),
    }
    return render(request, "404.html", ret)
