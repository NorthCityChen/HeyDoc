'''
Author: Mr.Sen
LastEditTime: 2021-02-16 10:52:01
Description: 
Website: https://grimoire.cn
Copyright (c) Mr.Sen All rights reserved.
'''
import re

regex = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'


string = 'https://www.runoob.com'
print("Urls: ", bool(re.match(regex, string)))
