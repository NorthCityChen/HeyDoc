'''
Author: Mr.Sen
LastEditTime: 2021-02-20 22:52:50
Description: 
Website: https://grimoire.cn
Copyright (c) Mr.Sen All rights reserved.
'''
# from freezer.upload import uploadMeta
# from freezer import upload
from django.conf.urls import handler400, handler404, handler500
from django.urls import path, re_path
# from django.contrib import admin

from . import user, error, distribution, showPages, upload

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('login/', user.loginApi),  # 登陆
    path('register/', user.registerApi),  # 注册
    path("upload/meta/", upload.uploadMeta),  # 生产分类信息
    path("upload/article/", upload.uploadArticle),  # 上传文章信息
    path("deleteArticle/", upload.deleteArticle),  # 删除文章
    path("renderEditor/", distribution.RenderEditor),  # 渲染编辑器
    path("metaOfUser/", distribution.DataForGroup),  # 渲染首页
    path("renderMeta/", distribution.RenderMeta),  # 渲染分组
    path("docOfMeta/", showPages.metaInfo),  # 展示分组页面
    path("doc/", showPages.loadArticle),  # 展示文章
    path("editor/", showPages.writePage),  # 展示编辑器
    path("", showPages.mainPage),  # 展示首页
    path("user/", showPages.userCenter), # 展示用户中心
    path("updateUserInfo/", upload.updateUserInfo), # 更新用户信息
    path("deleteMeta/", upload.deleteMeta), # 删除分类
    path("renameMeta/", upload.renameMeta), # 重命名分类
    path("metas/", showPages.metaCenter)
    # path("test/",upload.testApi),
    # re_path("^(?P<name>.*?)/(?P<docId>.*?)/$",article.loadArticle),
]

handler404 = error.pageNotFound
handler500 = error.serverErr